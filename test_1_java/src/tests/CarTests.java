/*Sirine Aoudj 1935903*/
package tests;
import vehicles.Car;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class CarTests {
	
	/*Testing if the getSpeed method returns the right value*/
	@Test
	void getSpeedTest() {
		Car testCar = new Car(5);
		assertEquals(5,testCar.getSpeed());
	}
	/*Testing if the getLocation returns the right value, it should give 0 since it has not been moved yet*/
	@Test
	void getLocationTest() {
		Car testCar = new Car(7);
		assertEquals(0,testCar.getLocation());
	}
	/*Testing if the move right test return the initial location + the speed*/
	@Test
	void moveRightTest() {
		Car testCar = new Car(11);
		testCar.moveRight();
		assertEquals(11, testCar.getLocation());
	}
	/*Testing if the move right test return the initial location + the speed*2 since we called the method twice*/
	@Test
	void moveRightTest2() {
		Car testCar = new Car(11);
		testCar.moveRight();
		testCar.moveRight();
		assertEquals(22, testCar.getLocation());
	}
	/*Testing if the move left test return the initial location - the speed*/
	@Test
	void moveLeftTest() {
		Car testCar = new Car(32);
		testCar.moveLeft();
		assertEquals(-32, testCar.getLocation());
	}
	/*Testing if the move left test return the initial location - the speed*2 since we called the method twice*/
	@Test
	void moveLeftTest2() {
		Car testCar = new Car(32);
		testCar.moveLeft();
		testCar.moveLeft();
		assertEquals(-64, testCar.getLocation());
	}
	/*Testing if the accelerate method will return the speed + 1*/
	@Test
	void accelerateTest() {
		Car testCar = new Car(10);
		testCar.accelerate();
		assertEquals(11,testCar.getSpeed());
	}
	/*Testing if the accelerate method will return the speed + 2, since we called the function twice*/
	@Test
	void accelerateTest2() {
		Car testCar = new Car(10);
		testCar.accelerate();
		testCar.accelerate();
		assertEquals(12,testCar.getSpeed());
	}
	/*Testing if the decelerate method will return the speed - 2 since we used the method twice*/
	@Test
	void decelerateTest() {
		Car testCar = new Car(10);
		testCar.decelerate();
		testCar.decelerate();
		assertEquals(8,testCar.getSpeed());
	}
	/*Testing if the decelerate method will return the speed as 0 since it should not change when the speed is equal to zero*/
	@Test
	void decelerateTest2() {
		Car testCar = new Car(0);
		testCar.decelerate();
		assertEquals(0,testCar.getSpeed());
	}
	/*Testing if the decelerate method will return the speed - 1*/
	@Test
	void decelerateTest3() {
		Car testCar = new Car(12);
		testCar.decelerate();
		assertEquals(11,testCar.getSpeed());
	}


}
