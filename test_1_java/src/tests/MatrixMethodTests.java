/*Sirine Aoudj 1935903*/
package tests;
import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class MatrixMethodTests{
	
	@Test
	void duplicateTest() {
	int arr[][] = { {1, 2}, {3, 4}, {5, 6}, {7, 8} };
	int arr2[][] = MatrixMethod.duplicate(arr);
	int expectedArr[][] = { {1, 2, 1, 2}, {3, 4, 3, 4}, {5, 6, 5, 6}, {7, 8, 7, 8} };
	assertArrayEquals(expectedArr, arr2);
	}
}
