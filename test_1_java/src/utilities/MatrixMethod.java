package utilities;

public class MatrixMethod {
	public class MatrixMethod1 {
		 public int[][] duplicate(int[][] array1){
		  int [][] newArray = new int[array1.length][array1[0].length*2];
		  for(int i = 0; i < array1.length; i++) {
		    int count = 0;
		   for(int j = 0; j < array1[i].length; j++) {
		     newArray[i][j] = array1[i][count];
		    count++;
		    if(count == array1[i].length){
		      count = 0;
		   }
		  }
		 }
		 return newArray;
		 }
	public static void main(String args) {
		int arr[][] = { {1, 2}, {3, 4}, {5, 6}, {7, 8} };
		int arr2[][] = duplicate(arr);
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr2[j].length; j++) {
			System.out.println(arr2[i][j]);
		}
	}

}
}
